Bug Tracker

## Jump start

Get started:

1. Clone the git repo — `git clone https://adrianstaniloiu@bitbucket.org/adrianstaniloiu/bugtracking.git` and checkout the tagged release you need.
2. Execute in the shell `sudo npm install` to install each node.js dependencies, needed for [Gulp.](http://gulpjs.com/)
3. Execute in the shell `bower install` to install each front-end dependencies.
4. Execute in the shell `gulp` to generate the stylesheets and scripts files in the `app/assets/` folder.
