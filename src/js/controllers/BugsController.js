app.controller('BugsCtrl', function($scope, Bugs) {
    $scope.seo.title = 'Bugs | Bugrr';
    $scope.seo.description = 'Bugs List';
    $scope.Bugs = Bugs;
    $scope.notSorted = function(obj) {
        if (!obj) {
            return [];
        }
        return Object.keys(obj);
    }
});