// Return list of bugs from ajax call.
app.factory('Bugs', function($http) {
    var Bugs = { "open": [],  "closed": [] };

    $http.get('bugs.json')
        .success(function (data) {
            for(i in data) {
                if (data[i].state === 'Closed') {
                    Bugs.closed.push(data[i]);
                } else {
                    Bugs.open.push(data[i]);
                }
            }
        })
        .error(function (data, status, headers, config) {
            Bugs.notfound = true;
        });

    return Bugs;
});

 